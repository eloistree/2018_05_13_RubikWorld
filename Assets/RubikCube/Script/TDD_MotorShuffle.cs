﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TDD_MotorShuffle : MonoBehaviour {

    public RubikCubeRotateMotor m_targetMotor;
    public int m_numberRotation=50;
	// Use this for initialization
	void Start () {
       
	}

    private void ShuffleFace(int timeToRotate = 20)
    {
        for (int i = 0; i < m_numberRotation; i++)
        {
            m_targetMotor.LocalRotate(GetRandomFace(), GetRandomDirection());
        }
    }

    private RubikCubeFace GetRandomFace()
    {
        int face = UnityEngine.Random.Range(0, 9);
        switch (face)
        {
            case 0: return RubikCubeFace.Back;
            case 1: return RubikCubeFace.Down;
            case 2: return RubikCubeFace.Equator;
            case 3: return RubikCubeFace.Face;
            case 4: return RubikCubeFace.Left;
            case 5: return RubikCubeFace.Middle;
            case 6: return RubikCubeFace.Right;
            case 7: return RubikCubeFace.Standing;
            case 8: return RubikCubeFace.Up;
            default:
                return RubikCubeFace.Middle;
        }
    }

    private bool GetRandomDirection()
    {
        return UnityEngine.Random.Range(0, 2) == 0;
    }
}
