﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RubikCubeRotateMotorDirect : RubikCubeRotateMotor
{
    public bool isRotating;


    public RubikCube m_affectedRubikCube;

 

    public override bool IsRotating()
    {
        return isRotating;
    }

    public override void LocalRotate(RubikCubeFace faceToRotate, bool clockwise)
    {
        isRotating = true;
        NotifyStartRotation(faceToRotate, clockwise);

        Transform pivot = m_affectedRubikCube.GetPivot(faceToRotate);
        RubikPiece[] pieces = m_affectedRubikCube.GetPieces(faceToRotate);
        float colockDirection = clockwise?1f:-1f ;
        foreach (RubikPiece piece in pieces)
        {
            piece.m_root.RotateAround(pivot.position, pivot.up, 90f * colockDirection);
        }

        NotifyEndRotation(faceToRotate, clockwise);
        isRotating = false;
    }

    public override void LocalRotate(RotationTypeShort faceToRotate)
    {
        switch (faceToRotate)
        {
            case RotationTypeShort.L:
                LocalRotate(RubikCubeFace.Left,true);
                break;
            case RotationTypeShort.Lp:
                LocalRotate(RubikCubeFace.Left, false);
                break;
            case RotationTypeShort.R:
                LocalRotate(RubikCubeFace.Right, true);
                break;
            case RotationTypeShort.Rp:
                LocalRotate(RubikCubeFace.Right, false);
                break;
            case RotationTypeShort.U:
                LocalRotate(RubikCubeFace.Up, true);
                break;
            case RotationTypeShort.Up:
                LocalRotate(RubikCubeFace.Up, false);
                break;
            case RotationTypeShort.D:
                LocalRotate(RubikCubeFace.Down, true);
                break;
            case RotationTypeShort.Dp:
                LocalRotate(RubikCubeFace.Down, false);
                break;
            case RotationTypeShort.F:
                LocalRotate(RubikCubeFace.Face, true);
                break;
            case RotationTypeShort.Fp:
                LocalRotate(RubikCubeFace.Face, false);
                break;
            case RotationTypeShort.B:
                LocalRotate(RubikCubeFace.Back, true);
                break;
            case RotationTypeShort.Bp:
                LocalRotate(RubikCubeFace.Back, false);
                break;
            case RotationTypeShort.M:
                LocalRotate(RubikCubeFace.Middle, true);
                break;
            case RotationTypeShort.Mp:
                LocalRotate(RubikCubeFace.Middle, false);
                break;
            case RotationTypeShort.E:
                LocalRotate(RubikCubeFace.Equator, true);
                break;
            case RotationTypeShort.Ep:
                LocalRotate(RubikCubeFace.Equator, false);
                break;
            case RotationTypeShort.S:
                LocalRotate(RubikCubeFace.Standing, true);
                break;
            case RotationTypeShort.Sp:
                LocalRotate(RubikCubeFace.Standing, false);
                break;
        }
    }


 
}
