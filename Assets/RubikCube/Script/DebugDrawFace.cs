﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugDrawFace : MonoBehaviour {

    public RubikCube m_rubikCube;
    public RubikCubeFace m_faceToDisplay;
    void Update () {
       switch (m_faceToDisplay)
        {
            case RubikCubeFace.Left:
                m_rubikCube.DebugDisplayFace(RubikCubeFace.Left, 0, Color.yellow);

                break;
            case RubikCubeFace.Right:
                m_rubikCube.DebugDisplayFace(RubikCubeFace.Right, 0, Color.white);

                break;
            case RubikCubeFace.Up:
                m_rubikCube.DebugDisplayFace(RubikCubeFace.Up, 0, new Color(0, 0, 1));

                break;
            case RubikCubeFace.Down:
                m_rubikCube.DebugDisplayFace(RubikCubeFace.Down, 0, Color.green);

                break;
            case RubikCubeFace.Face:
                m_rubikCube.DebugDisplayFace(RubikCubeFace.Face, 0, new Color(255f / 255f, 128f / 255f, 0));

                break;
            case RubikCubeFace.Back:
                m_rubikCube.DebugDisplayFace(RubikCubeFace.Back, 0, Color.red);

                break;
            case RubikCubeFace.Middle:
                m_rubikCube.DebugDisplayFace(RubikCubeFace.Middle, 0, Color.green);
      
                break;
            case RubikCubeFace.Equator:
                m_rubikCube.DebugDisplayFace(RubikCubeFace.Equator, 0, Color.green);
                break;
            case RubikCubeFace.Standing:
                m_rubikCube.DebugDisplayFace(RubikCubeFace.Standing, 0, Color.green);
                break;
            default:
                break;
        }
    }
	
}
