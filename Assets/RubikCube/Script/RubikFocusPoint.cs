﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RubikFocusPoint : MonoBehaviour {

    [Header("Params")]
    public Transform m_pointerRotation;
    public LayerMask m_pieceLayer = ~0;


    [Header("Debug")]
    public RubikPiece m_pieceFocused;

    
	
	void Update () {

        RaycastHit hit;
        bool isFocusingPiece = Physics.Raycast(m_pointerRotation.position, m_pointerRotation.forward,out hit, float.MaxValue, m_pieceLayer);


        if (isFocusingPiece)
        {
            RubikPiece piece = hit.collider.GetComponent<RubikPiece>();
            if (piece != null)
            {
                m_pieceFocused = piece;
            }
            else m_pieceFocused = null;
        }
        else m_pieceFocused = null;

        //Draw ray;
        if (isFocusingPiece)
            Debug.DrawLine(m_pointerRotation.position, hit.point, Color.green);
        else
            Debug.DrawRay(m_pointerRotation.position, m_pointerRotation.forward*1000, Color.red);



    }
}
