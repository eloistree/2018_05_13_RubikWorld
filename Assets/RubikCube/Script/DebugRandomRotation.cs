﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugRandomRotation : MonoBehaviour {

  
	void Update () {
        if (Input.GetKeyDown(KeyCode.R)) {

            RotateRandomly();
        }
        
	}

    public void RotateRandomly()
    {
        transform.rotation = Quaternion.Euler(UnityEngine.Random.Range(0, 360), UnityEngine.Random.Range(0, 360), UnityEngine.Random.Range(0, 360));
    }
}
