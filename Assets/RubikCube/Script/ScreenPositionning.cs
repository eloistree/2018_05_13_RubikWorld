﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenPositionning : MonoBehaviour {

    public Transform _toAffect;
    public Vector3 _offset;

    public Vector3 _worldPosition;
    public Vector3 _mousePosition;
    public Camera _usedCamera;
    public float _depth=0.5f;
	void Start () {


    }
	
	void Update () {

        _usedCamera = Camera.main;
        _mousePosition = Input.mousePosition;
        _mousePosition.z = _depth;
        _worldPosition = _usedCamera.ScreenToWorldPoint(_mousePosition);
        _toAffect.position = _worldPosition;
        _toAffect.rotation = _usedCamera.transform.rotation;
		
	}
}
