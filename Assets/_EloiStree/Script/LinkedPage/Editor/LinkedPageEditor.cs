﻿
using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(LinkedPage))]
public class LinkedPageEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        LinkedPage myScript = (LinkedPage)target;
        if (GUILayout.Button("Open"))
        {
            Application.OpenURL(myScript.m_url);
        }
    }
}