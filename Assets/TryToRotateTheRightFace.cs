﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TryToRotateTheRightFace : MonoBehaviour {

    [Header("Params")]
    public RubikCube m_rubikCube;
    public Transform m_refOrientationPoint;

    [Header("Debug")]
    public Transform m_root;
    public RubikCubePivot[] m_pivots;

    public RubikCubePivot m_left;
    public RubikCubePivot m_right;
    public RubikCubePivot m_up;
    public RubikCubePivot m_down;
    public RubikCubePivot m_face;
    public RubikCubePivot m_back;


    public RubikCubePivot m_equator;
    public RubikCubePivot m_middle;
    public RubikCubePivot m_standing;



    // Use this for initialization
    void Start () {

        m_pivots = m_rubikCube.m_pivots;

    }
	
	// Update is called once per frame
	void Update ()
    {
        m_face = GetFacePivot(m_refOrientationPoint);
        m_back = GetBackPivot(m_refOrientationPoint);
        m_left = GetLeftPivot(m_refOrientationPoint);
        m_right = GetRightPivot(m_refOrientationPoint);
        m_up = GetUpPivot(m_refOrientationPoint);
        m_down = GetDownPivot(m_refOrientationPoint);
        m_equator = GetEquatorPivot(m_refOrientationPoint);
        m_standing = GetStandingPivot(m_refOrientationPoint);
        m_middle = GetMiddlePivot(m_refOrientationPoint);

    }

    private RubikCubePivot GetLeftPivot(Transform m_refOrientationPoint)
    {
        return m_pivots.OrderBy(t => LeftDistanceOf(m_refOrientationPoint, t.transform.position)).First();
    }
    private RubikCubePivot GetRightPivot(Transform m_refOrientationPoint)
    {
        return m_pivots.OrderByDescending(t => LeftDistanceOf(m_refOrientationPoint, t.transform.position)).First();
    }

    private object LeftDistanceOf(Transform m_refOrientationPoint, Vector3 position)
    {
        Vector3 localPosition = m_refOrientationPoint.InverseTransformPoint(position);
        return localPosition.x;
    }




    private RubikCubePivot GetUpPivot(Transform m_refOrientationPoint)
    {
        return m_pivots.OrderByDescending(t => UpDistanceOf(m_refOrientationPoint, t.transform.position)).First();
    }
    private RubikCubePivot GetDownPivot(Transform m_refOrientationPoint)
    {
        return m_pivots.OrderBy(t => UpDistanceOf(m_refOrientationPoint, t.transform.position)).First();
    }
        
    private object UpDistanceOf(Transform m_refOrientationPoint, Vector3 position)
    {
        Vector3 localPosition = m_refOrientationPoint.InverseTransformPoint(position);
        return localPosition.y;
    }


    private RubikCubePivot GetFacePivot(Transform m_refOrientationPoint)
    {
        return m_pivots.OrderBy(t => FaceDistanceOf(m_refOrientationPoint, t.transform.position)).First();
    }
    private RubikCubePivot GetBackPivot(Transform m_refOrientationPoint)
    {
        return m_pivots.OrderByDescending(t => FaceDistanceOf(m_refOrientationPoint, t.transform.position)).First();
    }

    private object FaceDistanceOf(Transform m_refOrientationPoint, Vector3 position)
    {
        Vector3 localPosition = m_refOrientationPoint.InverseTransformPoint(position);
        return localPosition.z;
    }



    private RubikCubePivot GetEquatorPivot(Transform m_refOrientationPoint)
    {
        return m_pivots.Where(t=>t.m_isAtCenter).OrderByDescending(t => EquatorDistanceOf(m_refOrientationPoint, t.transform.up)).First();
    }

    private float EquatorDistanceOf(Transform m_refOrientationPoint, Vector3 upDirection)
    {
        Vector3 localDirection = m_refOrientationPoint.InverseTransformDirection(upDirection);
        return Mathf.Abs(localDirection.y);
    }

    private RubikCubePivot GetStandingPivot(Transform m_refOrientationPoint)
    {
        return m_pivots.Where(t => t.m_isAtCenter).OrderByDescending(t => StandingDistanceOf(m_refOrientationPoint, t.transform.up)).First();
    }

    private float StandingDistanceOf(Transform m_refOrientationPoint, Vector3 upDirection)
    {
        Vector3 localDirection = m_refOrientationPoint.InverseTransformDirection(upDirection);
        return Mathf.Abs(localDirection.z);
    }

    private RubikCubePivot GetMiddlePivot(Transform m_refOrientationPoint)
    {
        return m_pivots.Where(t => t.m_isAtCenter).OrderByDescending(t => MiddleDistanceOf(m_refOrientationPoint, t.transform.up)).First();
    }

    private float MiddleDistanceOf(Transform m_refOrientationPoint, Vector3 upDirection)
    {
        Vector3 localDirection = m_refOrientationPoint.InverseTransformDirection(upDirection);
        return Mathf.Abs(localDirection.x);
    }






    public Vector3 GetCartesianPosition(RubikCubePivot pivot) {
        return m_refOrientationPoint.InverseTransformPoint(pivot.m_root.position);
    }


    public void SetRotationWithAcronym(string acronym) {
        RotationTypeShort rotationType = RubikCube.ConvertStringToShortAcronym(acronym);
        bool clockWise;
        RubikCubeFace face;
        RubikCube.ConvertAcronymToFaceRotation(rotationType, out face, out clockWise);
        RubikCubeFace realFaceToRotate = GetRealFaceOf(face);
        m_rubikCube.LocalRotate(realFaceToRotate, true);

    }




    public RubikCubeFace GetRealFaceOf(RubikCubeFace faceChosen) {
        RubikCubePivot pivot=null;
        switch (faceChosen)
        {
            case RubikCubeFace.Left:
                pivot = m_left;
                break;
            case RubikCubeFace.Right:
                pivot = m_right;
                break;
            case RubikCubeFace.Up:
                pivot = m_up;
                break;
            case RubikCubeFace.Down:
                pivot = m_down;
                break;
            case RubikCubeFace.Face:

                pivot = m_face;
                break;
            case RubikCubeFace.Back:

                pivot = m_back;
                break;
            case RubikCubeFace.Middle:

                pivot = m_middle;
                break;
            case RubikCubeFace.Equator:
                pivot = m_equator;
                break;
            case RubikCubeFace.Standing:
                pivot = m_standing;
                break;
            default:
                break;
        }
        return pivot.m_face;

    }

}
